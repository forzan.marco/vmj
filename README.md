# About

vmj is a version manager for Java, that allows to quickly switch between your installed Java versions and to download and setup for you different JREs and JDKs.

For more info visit https://vmj.francescosorge.com/ 

# Installation

1. Globally install vmj via npm:

```bash
npm i -g vmj
```
2. Launch command 

```bash
vmj
```
3. Change your JAVA_HOME variable to point to: `your/home/folder/.vmj/java`

4. Change your env variable PATH to point to: `your/home/folder/.vmj/java/bin`

5. A reboot may be required depending on your OS in order to refresh the system's enviroment variables.
