#!/usr/bin/env node

import { Add } from './commands/add';
import { InitialSetup } from './initialSetup';
import { Install } from './commands/install';
import { List } from './commands/list';
import { Remove } from './commands/remove';
import { Use } from './commands/use';
import { Info } from './commands/info';

// Reads vmj's package.json for app version
const packageJson = require('../package.json');
const { Command } = require('commander');
const commands = new Command();

if (process.argv.length <= 2) {
  // main
  // Application header
  console.log('                  _ ');
  console.log('__   ___ __ ___  (_)');
  console.log("\\ \\ / / '_ ` _ \\ | |");
  console.log(' \\ V /| | | | | || |');
  console.log('  \\_/ |_| |_| |_|/ |');
  console.log('               |__/ ');
  console.log();
  // Prints vmj version
  console.log(`Version: ${packageJson.version}`);
  console.log();
}

InitialSetup();

commands
  .command('info')
  .description('Get path for env configuration')
  .action(Info);

commands
  .command('list [available]')
  .description('Get a list of all available JRE and JDK')
  .action(List);

commands
  .command('add <name> <path>')
  .description('Add a local Java installation to the list')
  .action(Add);

commands
  .command('install <id> <jre/jdk>')
  .description('Install a version of Java JRE or JDK')
  .action(Install);

commands
  .command('use <name>')
  .description('Select the version you want to use')
  .action(Use);

commands
  .command('remove <name>')
  .description('Remove an installed Java version')
  .action(Remove);

commands.parse(process.argv);
