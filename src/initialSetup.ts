import { Config } from './config/config';

/**
 * check if .vmj folder and version.json file exist or create it
 */
export const InitialSetup = () => {
  const fs = require('fs');

  if (!fs.existsSync(Config.vmj)) {
    fs.mkdirSync(Config.vmj, 0o744);
  }

  if (!fs.existsSync(Config.vmjInstallations)) {
    fs.mkdirSync(Config.vmjInstallations, 0o744);
  }

  if (!fs.existsSync(Config.versions)) {
    fs.writeFileSync(Config.versions, '[]');
  }
};
