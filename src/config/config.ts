const homedir = require('os').homedir();
const path = require('path');

export const Config = {
  vmj: path.join(homedir, '.vmj'),
  versions: path.join(homedir, '.vmj', 'versions.json'),
  java: path.join(homedir, '.vmj', 'java'),
  bin: path.join(homedir, '.vmj', 'java', 'bin'),
  vmjInstallations: path.join(homedir, '.vmj', 'installations'),
  apiVersions: 'https://vmj.francescosorge.com/api/version',
  apiDescription: 'https://vmj.francescosorge.com/api/version/',
};
