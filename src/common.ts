import { Config } from './config/config';
import IVersion from './interfaces/IVersion';

export const getVersionFile = async (): Promise<IVersion[]> => {
  const fs = await import('fs');
  return JSON.parse(fs.readFileSync(Config.versions, 'utf8'));
};

export const formatTableData = (value: boolean, effect: any) =>
  value ? 'Yes' : 'No';

export const selectedVersionData = (value: boolean) => (value ? '*' : '');
