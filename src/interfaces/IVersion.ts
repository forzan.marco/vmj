/**
 * Interface for the version.json data
 */
export default interface IVersion {
  name: string;
  path: string;
  selected: boolean;
  isJDK: boolean;
}
