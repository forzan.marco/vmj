export default interface IVersionAvailable {
  virtualId: string;
  name: string;
  version: string;
  idJDK: boolean;
  isLTS: boolean;
  supportedUntil: Date;
  isSupported: true;
}
