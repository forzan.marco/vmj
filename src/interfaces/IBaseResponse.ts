export default interface IBaseResponse<T> {
  date: Date;
  success: boolean;
  error: boolean;
  data: T;
}
