export default interface IDownloadUrls {
  os: string;
  platform: string;
  url: string;
  type: string;
}
