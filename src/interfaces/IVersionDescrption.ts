import IDownloadUrls from './IDownloadUrls';

export default interface IVersionDescription {
  downloadUrls: IDownloadUrls[];
  name: string;
  version: string;
  isJDK: boolean;
  isLTS: boolean;
  supportedUntil: string;
  virtualId: string;
  isSupported: boolean;
  id: string;
}
