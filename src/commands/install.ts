import { AxiosResponse } from 'axios';
import extractZip = require('extract-zip');
import prompts = require('prompts');
import { getVersionFile } from '../common';
import { Config } from '../config/config';
import IBaseResponse from '../interfaces/IBaseResponse';
import IVersion from '../interfaces/IVersion';
import IVersionDescription from '../interfaces/IVersionDescrption';

export const Install = async (virtualId: string, type: 'jdk' | 'jre') => {
  const os = require('os');
  const axios = require('axios');
  const isWin = os.platform() === 'win32';
  const isMacOs = os.platform() === 'darwin';
  const allJavaVersions = await getVersionFile();
  if (
    allJavaVersions.find(
      (version) =>
        version.name === `${virtualId}-${version.isJDK ? 'jdk' : 'jre'}`,
    )
  ) {
    const { accept } = await prompts({
      type: 'confirm',
      name: 'accept',
      initial: false,
      message: `A version of this ${type} is already installed. Do you want to reinstall it?`,
    });
    if (!accept) {
      return;
    }
  }
  axios
    .get(Config.apiDescription + virtualId)
    .then(
      async (response: AxiosResponse<IBaseResponse<IVersionDescription>>) => {
        if (response.data.success) {
          const url = response.data.data.downloadUrls.find(
            (version) =>
              version.os === os.platform() &&
              version.type === type &&
              version.platform === (os.arch() === 'ia32' ? 'x64' : os.arch()),
          )?.url;
          if (url !== undefined) {
            const Downloader = require('nodejs-file-downloader');

            const downloader = new Downloader({
              url,
              directory: os.tmpdir(),
              cloneFiles: false,
              fileName: isWin ? `${virtualId}.zip` : `${virtualId}.tar.gz`,
            });

            try {
              console.log('Downloading...');
              await downloader.download();
              console.debug('Downloaded. Extracting...');
              const path = await import('path');

              const fs = require('fs');
              const beforeFiles: string[] = [];
              for (const file of fs.readdirSync(Config.vmjInstallations)) {
                beforeFiles.push(file);
              }
              if (isWin) {
                await extractZip(path.join(os.tmpdir(), `${virtualId}.zip`), {
                  dir: Config.vmjInstallations,
                });
              } else {
                const decompress = require('decompress');
                const decompressTargz = require('decompress-targz');
                await decompress(
                  path.join(os.tmpdir(), `${virtualId}.tar.gz`),
                  Config.vmjInstallations,
                  {
                    plugins: [decompressTargz()],
                  },
                ).then(() => {
                  console.log('Files decompressed');
                });
              }

              console.log(`saved at: ${Config.vmjInstallations}`);

              const newFile = fs
                .readdirSync(Config.vmjInstallations)
                .find(
                  (newFileInstalled: string) =>
                    !beforeFiles.find(
                      (file: string) => file === newFileInstalled,
                    ) && !newFileInstalled.startsWith('._'),
                );

              if (newFile) {
                const newVersion: IVersion = {
                  name: `${virtualId}-${type}`,
                  path: path.join(
                    Config.vmjInstallations,
                    isMacOs ? path.join(newFile, 'Contents', 'Home') : newFile,
                  ),
                  isJDK: type === 'jdk',
                  selected: false,
                };

                allJavaVersions.push(newVersion);

                fs.writeFileSync(
                  Config.versions,
                  JSON.stringify(allJavaVersions),
                );
              }
            } catch (error) {
              console.error('Download failed', error);
            }
          } else {
            console.log('No version available for your system');
          }
        }
        if (response.data.error) {
          console.log(response.data.data);
        }
      },
    )
    .catch((error: any) => {
      console.log(error);
    });
};
