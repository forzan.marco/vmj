import { AxiosResponse } from 'axios';
import {
  formatTableData,
  getVersionFile,
  selectedVersionData,
} from '../common';
import { Config } from '../config/config';
import IBaseResponse from '../interfaces/IBaseResponse';
import IVersionAvailable from '../interfaces/IVersionAvailablle';

export const List = async (available: 'available') => {
  if (available !== undefined && available !== 'available') {
    return;
  }
  const Table = require('tty-table');
  if (available) {
    const axios = require('axios');

    const header = [
      {
        alias: 'ID',
        value: 'virtualId',
        headerColor: 'white',
        color: 'white',
        align: 'left',
        width: 20,
      },
      {
        alias: 'Name',
        value: 'name',
        headerColor: 'white',
        color: 'white',
        align: 'left',
        width: 20,
      },
      {
        alias: 'Version',
        value: 'version',
        headerColor: 'white',
        color: 'white',
        align: 'left',
        width: 10,
      },
      {
        alias: 'LTS',
        value: 'isLTS',
        headerColor: 'white',
        color: 'white',
        align: 'left',
        width: 10,
        formatter: formatTableData,
      },
      {
        alias: 'Supported until',
        value: 'supportedUntil',
        headerColor: 'white',
        color: 'white',
        align: 'left',
        width: 20,
        formatter: (value: string) => {
          return value.replace(/\T.+/, '');
        },
      },
      {
        alias: 'Supported',
        value: 'isSupported',
        headerColor: 'white',
        color: 'white',
        align: 'left',
        width: 12,
        formatter: formatTableData,
      },
    ];

    axios
      .get(Config.apiVersions)
      .then((response: AxiosResponse<IBaseResponse<IVersionAvailable>>) => {
        const out = Table(header, response.data.data).render();
        console.log(out);
      })
      .catch((error: any) => {
        console.log(error);
      });
  } else {
    const allJavaVersions = await getVersionFile();
    if (allJavaVersions.length === 0) {
      console.log('No versions installed');
    } else {
      const header = [
        {
          alias: 'Selected',
          value: 'selected',
          headerColor: 'white',
          color: 'white',
          align: 'right',
          formatter: selectedVersionData,
        },
        {
          alias: 'Name',
          value: 'name',
          headerColor: 'white',
          color: 'white',
          align: 'center',
        },
        {
          alias: 'JDK',
          value: 'isJDK',
          headerColor: 'white',
          color: 'white',
          align: 'center',
          formatter: formatTableData,
        },
        {
          alias: 'Path',
          value: 'path',
          headerColor: 'white',
          color: 'white',
          align: 'left',
        },
      ];

      console.log(Table(header, allJavaVersions).render());
    }
  }
};
