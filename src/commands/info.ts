import { Config } from '../config/config';

export const Info = () => {
  console.log(`JAVA_HOME=${Config.java}`);
  console.log(
    `PATH=${Config.bin}${process.platform === 'win32' ? ';%PATH%' : ':$PATH'}`,
  );
};
