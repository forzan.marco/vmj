import { getVersionFile } from '../common';
import { Config } from '../config/config';
import IVersion from '../interfaces/IVersion';

export const Use = async (name: string) => {
  const fs = require('fs');

  const allJavaVersions = await getVersionFile();

  if (
    allJavaVersions.find((version: IVersion) => version.name === name) ===
    undefined
  ) {
    console.log(`There is no version available with name ${name}`);
    return;
  }

  for (const version of allJavaVersions) {
    version.selected = version.name === name;

    if (version.name === name) {
      if (fs.existsSync(Config.java)) {
        fs.rmSync(Config.java, { recursive: true });
      }
      fs.symlinkSync(version.path, Config.java, 'junction');
    }
  }

  fs.writeFileSync(Config.versions, JSON.stringify(allJavaVersions));
  console.log(`Now using Java ${name}`);
};
