import { getVersionFile } from '../common';
import { Config } from '../config/config';
import IVersion from '../interfaces/IVersion';

export const Remove = async (name: string) => {
  const fs = require('fs');

  const allJavaVersions = await getVersionFile();
  const versionToDelete = allJavaVersions.find(
    (version: IVersion) => version.name === name,
  );

  if (versionToDelete?.selected) {
    console.log('the version you are trying to delete is in use');
  } else {
    if (versionToDelete) {
      allJavaVersions.splice(
        allJavaVersions.findIndex((version: IVersion) => version.name === name),
        1,
      );
      fs.writeFileSync(Config.versions, JSON.stringify(allJavaVersions));
      if (versionToDelete.path.startsWith(Config.vmjInstallations)) {
        fs.rmSync(versionToDelete.path, { recursive: true });
      }
    } else {
      console.log(`version with name ${name} not found`);
    }
  }
};
