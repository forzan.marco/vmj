import { getVersionFile } from '../common';
import { Config } from '../config/config';
import IVersion from '../interfaces/IVersion';

export const Add = async (name: string, javaPath: string) => {
  const fs = require('fs');
  const path = require('path');

  const newJavaVersion: IVersion = {
    name,
    path: javaPath,
    selected: false,
    isJDK: false,
  };

  const allJavaVersions = await getVersionFile();

  if (
    allJavaVersions.find(
      (version: IVersion) => version.name === newJavaVersion.name,
    )
  ) {
    console.log(
      `There is already an existing path with name ${newJavaVersion.name}`,
    );
    return;
  }

  const existingVersion = allJavaVersions.find(
    (version: IVersion) => version.path === newJavaVersion.path,
  );

  if (existingVersion) {
    console.log(
      `There is already an existing version with this path named ${existingVersion.name}`,
    );
    return;
  }
  if (fs.existsSync(Config.bin)) {
    if (
      !fs.existsSync(
        path.join(
          Config.bin,
          `java${process.platform === 'win32' ? '.exe' : ''}`,
        ),
      )
    ) {
      console.error('Given path missing java file');
      return;
    }

    if (
      fs.existsSync(
        path.join(
          Config.bin,
          `javac${process.platform === 'win32' ? '.exe' : ''}`,
        ),
      )
    ) {
      newJavaVersion.isJDK = true;
    }

    console.log('New Java version added');

    allJavaVersions.push(newJavaVersion);
    fs.writeFileSync(Config.versions, JSON.stringify(allJavaVersions));
  } else {
    console.error('Given path missing subdir bin');
  }
};
